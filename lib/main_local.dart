import 'package:flutter/material.dart';
import 'package:event/config/app_config.dart';
import 'package:event/event_app.dart';

void main() async {
  var configuredApp = AppConfig(
    flavor: Flavor.LOCAL,
    apiBaseUrl: 'http://localhost:4000',
    child: EventApp(),
  );
  await configuredApp.init();
  runApp(configuredApp);
}
