import 'dart:ui';

import 'package:flutter/material.dart';

class LiveColors {
  const LiveColors();

  static const Color accent = const Color(0xFFFBFFC1);
  static const Color primary = const Color(0xFF000000);

//  static const Color primaryGreen = const Color(0xFF00CA9D);
  static const Color primaryGreen = const Color(0xFF64bdcb);

  static const primaryGradient = const LinearGradient(
    colors: const [accent, primary],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}
