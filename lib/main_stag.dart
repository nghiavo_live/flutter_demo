import 'package:flutter/material.dart';
import 'package:event/config/app_config.dart';
import 'package:event/event_app.dart';

void main() async {
  var configuredApp = AppConfig(
    flavor: Flavor.STAG,
    apiBaseUrl: 'https://stag.event.nghiavo.live',
    child: EventApp(),
  );
  await configuredApp.init();
  runApp(configuredApp);
}
