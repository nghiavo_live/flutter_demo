import 'package:flutter/material.dart';

import 'config/app_config.dart';
import 'event_app.dart';

void main() async {
  var configuredApp = AppConfig(
    flavor: Flavor.PROD,
    apiBaseUrl: 'https://event.nghiavo.live',
    child: EventApp(),
  );
  await configuredApp.init();
  runApp(configuredApp);
}
