import 'dart:async';

import 'package:flutter/material.dart';
import 'package:event/di/dependency_injection.dart';
import 'package:meta/meta.dart';

enum Flavor { LOCAL, STAG, PROD }

class AppConfig extends InheritedWidget {
  AppConfig({
    @required this.flavor,
    @required this.apiBaseUrl,
    @required Widget child,
  }) : super(child: child);

  final Flavor flavor;
  final String apiBaseUrl;
  final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

  Future<void> init() async {
    var isLogin = await Injector.isLoginFt();
    Injector.configure(this);
    Injector.appBloc.inLoginState.add(isLogin);
  }

  static AppConfig of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
