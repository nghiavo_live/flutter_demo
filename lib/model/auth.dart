class TokenAuth {
  String token;

  TokenAuth({this.token});

  TokenAuth.fromJson(Map<String, dynamic> json) : token = json["token"];
}


class User {
  String id;
  String username;
  String email;

  User({this.id, this.username, this.email});

  User.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        username = json["username"],
        email = json[""];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['email'] = this.email;
    return data;
  }
}
