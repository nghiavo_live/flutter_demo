import 'dart:async';

import 'package:event/base/service.dart';
import 'package:event/model/auth.dart';

class AuthService extends BaseService {
  Future<TokenAuth> tokenAuth({username, password, accessToken}) async {
    var param = [];
    var json;
    if (accessToken?.isNotEmpty == true) {
      json = await post(
          'rest-auth/facebook${param.length > 0 ? '/?${param.join("\&")}' : ''}',
          data: {"access_token": accessToken});
    } else {
      json = await post(
          'token-auth${param.length > 0 ? '/?${param.join("\&")}' : ''}',
          data: {"username": username, "password": password});
    }
    return TokenAuth.fromJson(json);
  }

  Future<User> register({username, email, password}) async {
    var param = [];
    final json = await post(
        'register${param.length > 0 ? '/?${param.join("\&")}' : ''}',
        data: {"username": username, "password": password, "email": email});
    return User.fromJson(json);
  }

  Future<User> getProfile() async {
    var param = [];
    final json =
        await get('me${param.length > 0 ? '/?${param.join("\&")}' : ''}');
    return User.fromJson(json);
  }
}
