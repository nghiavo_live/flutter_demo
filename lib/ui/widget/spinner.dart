import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

class Spinner extends StatefulWidget {
  final IconData icon;
  final Color color;
  final Duration duration;

  const Spinner({
    Key key,
    @required this.icon,
    @required this.color,
    this.duration = const Duration(milliseconds: 1500),
  }) : super(key: key);

  @override
  SpinnerState createState() => SpinnerState();
}

class SpinnerState extends State<Spinner> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Widget _child;

  void start() {
    _controller.repeat();
  }

  void stop() {
    _controller.animateTo(1);
  }

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: widget.duration,
    )..repeat();
    _child = Icon(
      widget.icon,
      color: widget.color,
    );

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: _controller,
      child: _child,
    );
  }
}
