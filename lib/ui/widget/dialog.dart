import 'dart:io';

import 'package:flutter/material.dart';
import 'package:event/di/dependency_injection.dart';
import 'package:event/style/theme.dart' as Themes;

showUpdateDialog(
    BuildContext context, String newVersion, String changeLogs, bool require) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      title: Text(require
          ? "Please update to continue using app!"
          : "New version ($newVersion) avaiable!"),
      content: Text(changeLogs),
      actions: <Widget>[
        FlatButton(
          child: Text(require ? "Exit" : "Later"),
          onPressed: () {
            if (require) {
              exit(0);
            } else {
              Injector.navigator.goToHome(context);
            }
          },
        ),
        FlatButton(
          child: Text("Update now"),
          onPressed: () {
//            StoreRedirect.redirect();
          },
        ),
      ],
    ),
  );
}

showError(BuildContext context, String title, String message) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(title ?? "Opps!"),
      content: Text(message ?? "We hit a snag. Please try again in a bit."),
      actions: <Widget>[
        FlatButton(
          child: Text("Ok"),
          onPressed: () => Navigator.pop(context),
        )
      ],
    ),
  );
}

showComingSoon(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text("Hi"),
      content: Text(
          "This feature is coming soon. Please try again in the next version. Thank you!"),
      actions: <Widget>[
        FlatButton(
          child: Text("Ok"),
          onPressed: () => Navigator.pop(context),
        )
      ],
    ),
  );
}

showSuccess(BuildContext context, String message) {
  showDialog(
      context: context,
      builder: (context) => Center(
            child: Material(
              borderRadius: BorderRadius.circular(8.0),
              color: Colors.white,
              elevation: 5.0,
              child: Padding(
                padding: const EdgeInsets.all(32.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
//                    Icon(
//                      icon,
//                      color: Colors.green,
//                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      message,
                    )
                  ],
                ),
              ),
            ),
          ));
}

bool isShowingProgress = false;

showProgress(BuildContext context) {
  if (!isShowingProgress) {
//    Navigator.of(context).push(PageRouteBuilder(
//        opaque: false,
//        pageBuilder: (BuildContext context, _, __) {
//          return ProgressHUD(
//            backgroundColor: Colors.black12,
//            color: Themes.Colors.primaryGreen,
//            containerColor: Colors.black12,
//            borderRadius: 5.0,
//            text: "loading",
//          );
//        }));

    isShowingProgress = true;
  }
}

hideProgress(BuildContext context) {
  if (isShowingProgress) {
    Navigator.pop(context);
    isShowingProgress = false;
  }
}
