import 'package:flutter/material.dart';
import 'package:event/base/exception.dart';

class Status {
  String state; // loading, error, nothing
  BaseException ex;
  dynamic action;

  Status({this.state, this.ex, this.action});
}

class LoadingView extends StatefulWidget {
  final Status status;

  const LoadingView({Key key, this.status}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoadingViewState(status: this.status);
  }
}

class LoadingViewState extends State<LoadingView> {
  final Status status;

  LoadingViewState({this.status});

  void refreshState(state, ex, action) {
    setState(() {
      status.state = state;
      status.ex = ex;
      status.action = action;
    });
  }

  @override
  Widget build(BuildContext context) {
    final body = () {
      switch (status.state) {
        case "loading":
          return CircularProgressIndicator();
        case "nothing":
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Nothing to show", textAlign: TextAlign.center),
              FlatButton.icon(
                  onPressed: () {
                    setState(() {
                      status.state = "loading";
                    });
                    status.action();
                  },
                  icon: Icon(Icons.refresh),
                  label: Text("Tap to refresh"))
            ],
          );
        default:
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                status.ex.title,
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(status.ex.msg),
              FlatButton.icon(
                  onPressed: () {
                    setState(() {
                      status.state = "loading";
                    });
                    status.action();
                  },
                  icon: Icon(Icons.refresh),
                  label: Text("Tap to retry"))
            ],
          );
      }
    }();
    return Center(
      child: body,
    );
  }
}

LoadingView renderLoadingSate(GlobalKey<LoadingViewState> key,
    {state, ex, action}) {
  if (key != null && key.currentState != null) {
    key.currentState.refreshState(state, ex, action);
  }
  return LoadingView(
      key: key, status: Status(state: state, ex: ex, action: action));
}
