import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:event/style/theme.dart';
import 'package:flutter/material.dart';
import 'package:event/base/subscription.dart';
import 'package:event/di/dependency_injection.dart';
import 'package:event/util/snackbar.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends BaseState<HomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int bottomSelectedIndex = 0;

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  void _pageChangedAction(int index) {
    setState(() {
      bottomSelectedIndex = index;
    });
  }

  void _bottomTappedAction(int index) {
    setState(() {
      pageController.jumpToPage(index);
    });
  }

  @override
  void initState() {
    super.initState();
    sub(Injector.appBloc.outConnectionState.listen((result) {
      showConnectionStateSnackBar(result, _scaffoldKey.currentState);
    }));
  }

  @override
  Widget build(BuildContext context) {
    final bottomNavyBar = BubbleBottomBar(
      opacity: .2,
      currentIndex: bottomSelectedIndex,
      onTap: _bottomTappedAction,
      borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
      elevation: 8,
      //new
      hasInk: true,
      //new, gives a cute ink effect
      inkColor: Colors.black12,
      //optional, uses theme color if not specified
      items: <BubbleBottomBarItem>[
        BubbleBottomBarItem(
            backgroundColor: LiveColors.primaryGreen,
            icon: Icon(
              Icons.dashboard,
              color: Colors.grey,
            ),
            activeIcon: Icon(
              Icons.dashboard,
              color: LiveColors.primaryGreen,
            ),
            title: Text("Home")),
        BubbleBottomBarItem(
            backgroundColor: LiveColors.primaryGreen,
            icon: Icon(
              Icons.scatter_plot,
              color: Colors.grey,
            ),
            activeIcon: Icon(
              Icons.scatter_plot,
              color: LiveColors.primaryGreen,
            ),
            title: Text("Discover")),
        BubbleBottomBarItem(
            backgroundColor: LiveColors.primaryGreen,
            icon: Icon(
              Icons.notifications,
              color: Colors.grey,
            ),
            activeIcon: Icon(
              Icons.notifications,
              color: LiveColors.primaryGreen,
            ),
            title: Text("Notification")),
        BubbleBottomBarItem(
            backgroundColor: LiveColors.primaryGreen,
            icon: Icon(
              Icons.menu,
              color: Colors.grey,
            ),
            activeIcon: Icon(
              Icons.menu,
              color: LiveColors.primaryGreen,
            ),
            title: Text("Menu"))
      ],
    );

    final body = PageView(
      controller: pageController,
      onPageChanged: _pageChangedAction,
      children: <Widget>[
        Text(
          'You have pushed the button this many times:' +
              bottomSelectedIndex.toString(),
        ),
        Text(
          'You have pushed the button this many times:' +
              bottomSelectedIndex.toString(),
        ),
        Text(
          'You have pushed the button this many times:' +
              bottomSelectedIndex.toString(),
        ),
        Text(
          'You have pushed the button this many times:' +
              bottomSelectedIndex.toString(),
        ),
      ],
    );

    return Scaffold(
      key: _scaffoldKey,
      body: body,
      bottomNavigationBar: bottomNavyBar,
    );
  }
}
