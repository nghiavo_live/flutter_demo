import 'package:validate/validate.dart'; // Add import for validate package.

class Validator {
  /*
    Validate auth
   */
  static String validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'Please input a valid email address';
    }

    return null;
  }

  static String validateUsername(String value) {
    if (value.isEmpty) {
      return "Please input username";
    }

    return null;
  }

  static String validatePassword(String value) {
    if (value.isEmpty) {
      return "Please input password";
    }

    return null;
  }

  static String validateConfirmPassword(String value) {
    if (value.isEmpty) {
      return "Please re-confirm password";
    }

    return null;
  }

  /*
   Validate board
   */
  static String validateBoardName(String value) {
    if (value.isEmpty) {
      return "Please input group name";
    }

    return null;
  }

  static String validateQuoteContent(String value) {
    if (value.isEmpty) {
      return "Please input quote content";
    }

    return null;
  }

  /*
   Validate event
   */
  static String validateEventName(String value) {
    if (value.isEmpty) {
      return "Please input event name";
    }

    return null;
  }
}
