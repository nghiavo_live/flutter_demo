import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:event/di/dependency_injection.dart';

class DataKey {
  static final apiToken = "apiToken";
  static final profile = "profile";
  static final dailyQuoteTime = "dailyQuoteTime";
  static final favoriteTag = "favoriteTag";
}

R lookup<R, K>(Map<K, dynamic> map, Iterable<K> keys, [R defaultTo]) {
  dynamic current = map;
  for (final key in keys) {
    if (current is Map<K, dynamic>) {
      current = current[key];
    } else {
      return defaultTo;
    }
  }
  return current as R;
}

String imageUrl(path) =>
    path?.isEmpty == false ? "${Injector.apiBaseUrl}$path" : null;

Future<Uint8List> capturePng(GlobalKey _globalKey) async {
  try {
    RenderRepaintBoundary boundary =
        _globalKey.currentContext.findRenderObject();
    final image = await boundary.toImage(pixelRatio: 3.0);
    ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
    var pngBytes = byteData.buffer.asUint8List();
    return pngBytes;
  } catch (e) {
    return null;
  }
}
