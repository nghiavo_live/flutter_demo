import 'dart:async';

import 'package:event/ui/home/home_page.dart';
import 'package:flutter/material.dart';

class Navigation {
  Future goToHome(BuildContext context) {
    final homePage = HomePage();
    return Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
      return homePage;
    }));
  }
}
