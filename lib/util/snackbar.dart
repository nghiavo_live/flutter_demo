import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:event/ui/widget/snackbar.dart';

void showConnectionStateSnackBar(state, scaffold) {
  if (scaffold == null) {
    return;
  }

  if (state == ConnectivityResult.none) {
    showErrorSnackBar(scaffold.context,
        "You have lost connection to the internet. Please check it!",
        icon: Icon(
          Icons.signal_wifi_off,
          color: Colors.red[300],
        ));
  } else {
    showSuccessSnackBar(scaffold.context,
        "Connected to the internet. You can continue using the app now!",
        icon: Icon(
          Icons.signal_wifi_4_bar,
          size: 28.0,
          color: Colors.green[300],
        ));
  }
}
