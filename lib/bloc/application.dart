import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:event/base/bloc_provider.dart';
import 'package:event/di/dependency_injection.dart';
import 'package:rxdart/rxdart.dart';

class ApplicationBloc implements BaseBloc {
  StreamSubscription<ConnectivityResult> _connectionStateSub;

  PublishSubject<ConnectivityResult> _connectionStateController =
      PublishSubject<ConnectivityResult>();

  Sink<ConnectivityResult> get _inConnectionState =>
      _connectionStateController.sink;

  Stream<ConnectivityResult> get outConnectionState =>
      _connectionStateController.stream;

  BehaviorSubject<bool> _loginStateController = BehaviorSubject<bool>();

  Sink<bool> get inLoginState => _loginStateController.sink;

  Stream<bool> get outLoginState => _loginStateController.stream;

  BehaviorSubject<BuildContext> _requireLoginController =
      BehaviorSubject<BuildContext>();

  Sink<BuildContext> get inRequireLogin => _requireLoginController.sink;


  ApplicationBloc() {
    _requireLoginController.stream.listen((context) {
//      Injector.navigator.gotToLoginPage(context);
    });

    _connectionStateSub = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      _inConnectionState.add(result);
    });
  }

  bool shouldRefreshBannerAd = false;


  Future dispose() async {
    _loginStateController.close();
    _requireLoginController.close();
    _connectionStateController.close();
    _connectionStateSub.cancel();
  }
}
