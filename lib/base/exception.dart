import 'package:event/di/dependency_injection.dart';

class BaseException implements Exception {
  final msg;

  final title;

  final code;

  BaseException({this.msg, this.title, this.code}) {
    if (this.code == 401 || this.code == "401") {
      Injector.logout();
    }
  }
}

BaseException getBaseException(status) {
  if (status is BaseException) {
    return status;
  }
  if (status is Exception || status is Error) {
    status = Map();
  }
  return BaseException(
      title: status['title'] ?? "Oops!",
      msg: status['msg'] ?? "We hit a snag. Please try again in a bit.",
      code: status['code'] ?? 1000);
}
