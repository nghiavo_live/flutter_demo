import 'package:flutter/material.dart';

abstract class BaseBloc {
  void dispose();
}

class BlocProvider extends StatefulWidget {
  BlocProvider({
    Key key,
    @required this.child,
    @required this.bloc,
  }) : super(key: key);

  final List<BaseBloc> bloc;
  final Widget child;

  @override
  BlocProviderState createState() => BlocProviderState();

  static T of<T extends BaseBloc>(BuildContext context) {
    final type = _typeOf<BlocProvider>();
    BlocProvider provider = context.ancestorWidgetOfExactType(type);
    T result;
    provider.bloc.forEach((bloc) {
      if (bloc is T) {
        result = bloc;
      }
    });
    return result;
  }

  static Type _typeOf<T>() => T;
}

class BlocProviderState extends State<BlocProvider> {
  @override
  void dispose() {
    widget.bloc.forEach((bloc) => bloc.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
