import 'dart:async';

import 'package:flutter/material.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  List<StreamSubscription> subs;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    if (subs != null) {
      subs.forEach((sub) => sub.cancel());
    }
  }

  void sub(StreamSubscription sub) {
    if (subs == null) {
      subs = new List();
    }
    subs.add(sub);
  }
}
