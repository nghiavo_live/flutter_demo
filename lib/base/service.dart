import 'dart:io';

import 'package:event/base/exception.dart';
import 'package:event/di/dependency_injection.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';

abstract class BaseService {
  String _url(endpoint) => endpoint.startsWith('http')
      ? endpoint
      : '${Injector.apiBaseUrl}/api/$endpoint${endpoint.contains('?') ? '' : '/'}';

  dynamic get(endpoint, {shouldSkipAuth = false}) async {
    try {
      final http.Response response = await http
          .get(_url(endpoint), headers: await _getHeader(shouldSkipAuth))
          .timeout(Duration(seconds: 30));
      if (response.statusCode >= 200 && response.statusCode < 300) {
        String body = utf8.decode(response.bodyBytes);
        return json.decode(body);
      } else {
        // If that call was not successful, throw an error.
        if (response.body?.isNotEmpty == true) {
          String body = utf8.decode(response.bodyBytes);
          var status = json.decode(body);
          throw getBaseException(status);
        } else {
          throw getBaseException(null);
        }
      }
    } catch (ex) {
      throw getBaseException(ex);
    }
  }

  dynamic post(endpoint, {data, shouldSkipAuth = false}) async {
    try {
      final http.Response response = await http
          .post(
            _url(endpoint),
            body: json.encode(data),
            headers: await _getHeader(shouldSkipAuth),
          )
          .timeout(Duration(seconds: 30));
      if (response.statusCode >= 200 && response.statusCode < 300) {
        String body = utf8.decode(response.bodyBytes);
        return json.decode(body);
      } else {
        // If that call was not successful, throw an error.
        if (response.body?.isNotEmpty == true) {
          String body = utf8.decode(response.bodyBytes);
          var status = json.decode(body);
          throw getBaseException(status);
        } else {
          throw getBaseException(null);
        }
      }
    } catch (ex) {
      throw getBaseException(ex);
    }
  }

  dynamic put(endpoint, {data, shouldSkipAuth = false}) async {
    try {
      final http.Response response = await http
          .put(_url(endpoint),
              body: json.encode(data),
              headers: await _getHeader(shouldSkipAuth))
          .timeout(Duration(seconds: 30));
      if (response.statusCode >= 200 && response.statusCode < 300) {
        String body = utf8.decode(response.bodyBytes);
        return json.decode(body);
      } else {
        // If that call was not successful, throw an error.
        if (response.body?.isNotEmpty == true) {
          String body = utf8.decode(response.bodyBytes);
          var status = json.decode(body);
          throw getBaseException(status);
        } else {
          throw getBaseException(null);
        }
      }
    } catch (ex) {
      throw getBaseException(ex);
    }
  }

  dynamic delete(endpoint, {data, shouldSkipAuth = false}) async {
    try {
      final http.Response response = await http
          .delete(_url(endpoint), headers: await _getHeader(shouldSkipAuth))
          .timeout(Duration(seconds: 30));
      if (response.statusCode >= 200 && response.statusCode < 300) {
        String body = utf8.decode(response.bodyBytes);
        if (body?.isNotEmpty == true) {
          return json.decode(body);
        }
        return null;
      } else {
        // If that call was not successful, throw an error.
        if (response.body?.isNotEmpty == true) {
          String body = utf8.decode(response.bodyBytes);
          var status = json.decode(body);
          throw getBaseException(status);
        } else {
          throw getBaseException(null);
        }
      }
    } catch (ex) {
      throw getBaseException(ex);
    }
  }

  dynamic _getHeader(shouldSkipAuth) async {
    final header = {"Content-Type": "application/json"};
    if (!shouldSkipAuth) {
      final apiToken = await Injector.apiToken;
      if (apiToken?.isNotEmpty == true) {
        header["Authorization"] = "Token $apiToken";
      }
    }
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String versionName = packageInfo.version;
    header['app-version'] = versionName;
    String versionCode = packageInfo.buildNumber;
    header['app-version-code'] = versionCode;
    String platform = Platform.isIOS ? "ios" : "droid";
    header['quote-platform'] = platform;

    return header;
  }
}
