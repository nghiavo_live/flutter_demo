import 'package:event/style/theme.dart';
import 'package:event/ui/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:event/base/bloc_provider.dart';
import 'package:event/config/app_config.dart';
import 'package:event/di/dependency_injection.dart';
import 'package:event/util/translations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class EventApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var config = AppConfig.of(context);

    return BlocProvider(
      bloc: [Injector.appBloc],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateTitle: (BuildContext context) => Translations.of(context).text(
            "app_name_${config.flavor.toString().split(".")[1].toLowerCase()}"),
        theme: ThemeData(
            primaryColor: LiveColors.primary,
            accentColor: LiveColors.primary,
            primaryTextTheme: Theme.of(context)
                .primaryTextTheme
                .apply(bodyColor: Colors.white),
            accentIconTheme:
                Theme.of(context).accentIconTheme.copyWith(color: Colors.white),
            primaryIconTheme: Theme.of(context)
                .accentIconTheme
                .copyWith(color: Colors.white)),
        home: HomePage(),
        navigatorObservers: [config.routeObserver],
        localizationsDelegates: [
          const TranslationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale("en", "US"),
          const Locale("vi", "VN"),
        ],
      ),
    );
  }
}
