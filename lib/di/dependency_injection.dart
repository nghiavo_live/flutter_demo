//Simple DI
import 'dart:async';
import 'dart:convert';

import 'package:event/service/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:event/bloc/application.dart';
import 'package:event/config/app_config.dart';
import 'package:event/model/auth.dart';
import 'package:event/util/data.dart';
import 'package:event/util/navigate.dart';

class Injector {
  static final Injector _singleton = Injector._internal();
  static FlutterSecureStorage storage = FlutterSecureStorage();
  static Navigation navigator;
  static String apiBaseUrl;
  static Flavor flavor;
  static ApplicationBloc appBloc;
  static bool isLogin = false;
  static String bannerAdId;
  static String interstitialAdId;
  static RouteObserver<PageRoute> routeObserver;

  static void configure(AppConfig config) async {
    apiBaseUrl = config.apiBaseUrl;
    flavor = config.flavor;
    routeObserver = config.routeObserver;
    navigator = Navigation();
    appBloc = ApplicationBloc();
  }

  factory Injector() => _singleton;

  Injector._internal();

  static Future<bool> isLoginFt() async {
    final result = await Injector.storage.read(key: DataKey.apiToken);
    isLogin = result?.isNotEmpty == true;
    return isLogin;
  }

  static bool isLoginRequire(BuildContext context) {
    if (!isLogin) {
      appBloc.inRequireLogin.add(context);
    }
    return isLogin;
  }

  static logout() async {
    await Injector.storage.deleteAll();
    Injector.appBloc.inLoginState.add(false);
  }

  static Future<String> get apiToken =>
      Injector.storage.read(key: DataKey.apiToken);

  static Future<User> get userProfile => Injector.storage
      .read(key: DataKey.profile)
      .then<User>((user) => User.fromJson(json.decode(user)));

  // Service di
  static AuthService authService = AuthService();
}
